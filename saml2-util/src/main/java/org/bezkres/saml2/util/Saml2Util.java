/*
 * (C) Copyright 2013 drodak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package org.bezkres.saml2.util;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import javax.security.auth.callback.UnsupportedCallbackException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.app.VelocityEngine;
import org.joda.time.DateTime;
import org.opensaml.Configuration;
import org.opensaml.DefaultBootstrap;
import org.opensaml.common.SAMLObject;
import org.opensaml.common.SAMLVersion;
import org.opensaml.common.binding.BasicSAMLMessageContext;
import org.opensaml.common.binding.BindingException;
import org.opensaml.common.impl.SecureRandomIdentifierGenerator;
import org.opensaml.common.xml.SAMLConstants;
import org.opensaml.saml1.binding.encoding.HTTPPostEncoder;
import org.opensaml.saml2.binding.decoding.HTTPPostDecoder;
import org.opensaml.saml2.binding.decoding.HTTPRedirectDeflateDecoder;
import org.opensaml.saml2.binding.encoding.HTTPRedirectDeflateEncoder;
import org.opensaml.saml2.core.Artifact;
import org.opensaml.saml2.core.ArtifactResolve;
import org.opensaml.saml2.core.ArtifactResponse;
import org.opensaml.saml2.core.Assertion;
import org.opensaml.saml2.core.Attribute;
import org.opensaml.saml2.core.AttributeStatement;
import org.opensaml.saml2.core.Audience;
import org.opensaml.saml2.core.AudienceRestriction;
import org.opensaml.saml2.core.AuthnRequest;
import org.opensaml.saml2.core.AuthnStatement;
import org.opensaml.saml2.core.Conditions;
import org.opensaml.saml2.core.Issuer;
import org.opensaml.saml2.core.LogoutRequest;
import org.opensaml.saml2.core.LogoutResponse;
import org.opensaml.saml2.core.NameID;
import org.opensaml.saml2.core.Response;
import org.opensaml.saml2.core.SessionIndex;
import org.opensaml.saml2.core.Status;
import org.opensaml.saml2.core.StatusCode;
import org.opensaml.saml2.core.Subject;
import org.opensaml.saml2.core.SubjectConfirmation;
import org.opensaml.saml2.core.SubjectConfirmationData;
import org.opensaml.saml2.core.impl.AuthnRequestBuilder;
import org.opensaml.saml2.core.impl.IssuerBuilder;
import org.opensaml.saml2.metadata.AssertionConsumerService;
import org.opensaml.saml2.metadata.Endpoint;
import org.opensaml.saml2.metadata.EntityDescriptor;
import org.opensaml.saml2.metadata.KeyDescriptor;
import org.opensaml.saml2.metadata.NameIDFormat;
import org.opensaml.saml2.metadata.SPSSODescriptor;
import org.opensaml.saml2.metadata.SingleLogoutService;
import org.opensaml.saml2.metadata.SingleSignOnService;
import org.opensaml.saml2.metadata.impl.AssertionConsumerServiceBuilder;
import org.opensaml.saml2.metadata.impl.EntityDescriptorBuilder;
import org.opensaml.saml2.metadata.impl.KeyDescriptorBuilder;
import org.opensaml.saml2.metadata.impl.NameIDFormatBuilder;
import org.opensaml.saml2.metadata.impl.SPSSODescriptorBuilder;
import org.opensaml.saml2.metadata.impl.SingleLogoutServiceBuilder;
import org.opensaml.saml2.metadata.impl.SingleSignOnServiceBuilder;
import org.opensaml.util.URLBuilder;
import org.opensaml.ws.message.decoder.MessageDecodingException;
import org.opensaml.ws.message.encoder.MessageEncodingException;
import org.opensaml.ws.soap.client.BasicSOAPMessageContext;
import org.opensaml.ws.soap.client.http.HttpClientBuilder;
import org.opensaml.ws.soap.client.http.HttpSOAPClient;
import org.opensaml.ws.soap.client.http.HttpSOAPRequestParameters;
import org.opensaml.ws.soap.soap11.Body;
import org.opensaml.ws.soap.soap11.Envelope;
import org.opensaml.ws.soap.soap11.impl.BodyBuilder;
import org.opensaml.ws.soap.soap11.impl.EnvelopeBuilder;
import org.opensaml.ws.transport.http.HttpServletRequestAdapter;
import org.opensaml.ws.transport.http.HttpServletResponseAdapter;
import org.opensaml.xml.ConfigurationException;
import org.opensaml.xml.XMLObject;
import org.opensaml.xml.XMLObjectBuilderFactory;
import org.opensaml.xml.io.Marshaller;
import org.opensaml.xml.io.MarshallingException;
import org.opensaml.xml.parse.BasicParserPool;
import org.opensaml.xml.security.SecurityException;
import org.opensaml.xml.security.SecurityHelper;
import org.opensaml.xml.security.credential.Credential;
import org.opensaml.xml.security.credential.UsageType;
import org.opensaml.xml.security.keyinfo.KeyInfoGenerator;
import org.opensaml.xml.security.x509.X509KeyInfoGeneratorFactory;
import org.opensaml.xml.signature.Signature;
import org.opensaml.xml.signature.SignatureException;
import org.opensaml.xml.signature.SignatureValidator;
import org.opensaml.xml.signature.Signer;
import org.opensaml.xml.util.DatatypeHelper;
import org.opensaml.xml.util.Pair;
import org.opensaml.xml.validation.ValidationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class Saml2Util {

	private static Log log = LogFactory.getLog(Saml2Util.class);
	private static Saml2Util instance = new Saml2Util();
	
	private static final long MILLIS_PER_MINUTE = 60 * 1000;
	
	public static Saml2Util getInstance() {
		return instance;
	}
	
	private Saml2Util() {
		try {
			log.debug("Bootstraping OpenSAML library");
			DefaultBootstrap.bootstrap();
		} catch (ConfigurationException ce) {
			log.error("Cannot bootstrap OpenSAML", ce);
			throw new RuntimeException("Cannot bootstrap OpenSAML", ce);
		}
	}

	public void sendAuthnRequest(Saml2SpConfig config, HttpServletRequest request,
			HttpServletResponse response, String relayState) throws Saml2Exception {
		AuthnRequest authRequest = buildAuthnRequest(config);

		// send request
		Endpoint ssoEndpoint = buildSsoRequestEndpoint(
				config.getIdpSsoServiceBinding(),
				config.getIdpSsoServiceLocation());
		if (SAMLConstants.SAML2_POST_BINDING_URI.equals(config
				.getIdpSsoServiceBinding())) {
			try {
				sendPostMessage(authRequest, relayState, ssoEndpoint, response);
			} catch (Exception e) {
				throw new Saml2Exception("Unable to send POST Authn Request", e);
			}
		} else if (SAMLConstants.SAML2_REDIRECT_BINDING_URI.equals(config.getIdpSsoServiceBinding())) {
			try {
				sendRedirectMessage(authRequest, relayState, ssoEndpoint, response);
 			} catch (Exception e) {
				throw new Saml2Exception("Unable to send REDIRECT Authn Request", e);
			}
		} else {
			throw new Saml2Exception("Unsupported SSO service binding: "
					+ config.getIdpSsoServiceBinding());
		}
	}
	
	private Endpoint buildSsoRequestEndpoint(String binding, String location) {
		XMLObjectBuilderFactory builderFactory = Configuration.getBuilderFactory();
		SingleSignOnServiceBuilder ssoBuilder = (SingleSignOnServiceBuilder) builderFactory
				.getBuilder(SingleSignOnService.DEFAULT_ELEMENT_NAME);
		SingleSignOnService sso = ssoBuilder.buildObject();
		sso.setBinding(binding);
		sso.setLocation(location);
		return sso;
	}
	
	private AuthnRequest buildAuthnRequest(Saml2SpConfig config) throws Saml2Exception {
		XMLObjectBuilderFactory builderFactory = Configuration
				.getBuilderFactory();
		AuthnRequestBuilder arb = (AuthnRequestBuilder) builderFactory
				.getBuilder(AuthnRequest.DEFAULT_ELEMENT_NAME);
		AuthnRequest authnRequest = (AuthnRequest) arb.buildObject();
		IssuerBuilder ib = (IssuerBuilder) builderFactory
				.getBuilder(Issuer.DEFAULT_ELEMENT_NAME);
		// Build the Issuer object
		Issuer issuer = ib.buildObject();
		issuer.setValue(config.getSpEntityId());
		authnRequest.setIssuer(issuer);
		authnRequest.setProviderName(config.getSpEntityId());
		authnRequest.setDestination(config.getIdpSsoServiceLocation());
		authnRequest.setAssertionConsumerServiceURL(config.getSpAcsLocation());
		authnRequest.setProtocolBinding(config.getSpAcsBinding());
		Integer attributeConsumingServiceIndex = config.getSpAttributeConsumingServiceIndex();
		if (attributeConsumingServiceIndex != null) {
			authnRequest.setAttributeConsumingServiceIndex(attributeConsumingServiceIndex);
		}
		if (config.isSpForceReAuthentication()) {
			authnRequest.setForceAuthn(true);
		}
		authnRequest.setVersion(SAMLVersion.VERSION_20);
		DateTime dt = new DateTime();
		authnRequest.setIssueInstant(dt);
		SecureRandomIdentifierGenerator secIdGen;
		try {
			secIdGen = new SecureRandomIdentifierGenerator();
		} catch (NoSuchAlgorithmException e) {
			throw new Saml2Exception("Unable to initialize id generator", e);
		}
		String id = secIdGen.generateIdentifier();
		authnRequest.setID(id);

		if (config.isIdpSsoWantsRequestsSigned()) {
			Signature signature = (Signature) builderFactory
					.getBuilder(Signature.DEFAULT_ELEMENT_NAME);
			authnRequest.setSignature(signature);
			try {
				SecurityHelper.prepareSignatureParams(signature,
						config.getSpSigningCredential(), null, null);
				Signer.signObject(signature);
			} catch (Exception e) {
				throw new Saml2Exception("Unable to sign AuthnReqest", e);
			}
			
		}

		return authnRequest;
	}

	protected String encodeRedirectURL(String endpointURL, String message,
			String relayState) throws Saml2Exception {
		log.debug("Building URL to redirect client to");
		URLBuilder urlBuilder = new URLBuilder(endpointURL);

		List<Pair<String, String>> queryParams = urlBuilder.getQueryParams();
		queryParams.clear();
		queryParams.add(new Pair<String, String>("SAMLRequest", message));
		if (checkRelayState(relayState)) {
			queryParams.add(new Pair<String, String>("RelayState", relayState));
		}
		return urlBuilder.buildURL();
	}

	protected boolean checkRelayState(String relayState) {
		if (!DatatypeHelper.isEmpty(relayState)) {
			if (relayState.getBytes().length > 80) {
				log.warn("Relay state exceeds 80 bytes, some application may not support this.");
			}
			return true;
		}
		return false;
	}

	public Assertion processAcsArtifact(Saml2SpConfig config, HttpServletRequest request,
			HttpServletResponse response) throws Saml2Exception {
		// decode artifact
		if (!"GET".equals(request.getMethod())) {
			log.debug("Unimplemented ACS request method: "
					+ request.getMethod());
			try {
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			} catch (IOException e) {
				throw new Saml2Exception("Unable to send error", e);
			}
		}
		String encodedArtifact = DatatypeHelper
				.safeTrimOrNullString((String) request.getParameter("SAMLart"));
		if (encodedArtifact == null) {
			throw new Saml2Exception("No SAMLart attribute value found");
		}
		// get assertion from ARS
		Response authResponse = resolveArtifact(config, encodedArtifact);
		// validate assertion and setup SAML2 session
		Assertion assertion = validateAuthnResponse(config, authResponse);

		return assertion;
	}

	public Assertion processAcsPost(Saml2SpConfig config, HttpServletRequest request,
			HttpServletResponse response) throws Saml2Exception {
		HTTPPostDecoder decode = new HTTPPostDecoder(new BasicParserPool());
		HttpServletRequestAdapter adapter = new HttpServletRequestAdapter(
				request);
		BasicSAMLMessageContext context = new BasicSAMLMessageContext();
		context.setInboundMessageTransport(adapter);
		try {
			decode.decode(context);
		} catch (Exception e) {
			throw new Saml2Exception("Unable to decode SAML message", e);
		}
		// get & validate Assertion
		Response authnResponse = (Response) context.getInboundMessage();

		// process assertion to create SAML2 session
		Assertion assertion = validateAuthnResponse(config, authnResponse);
		
		return assertion;
	}

	private Response resolveArtifact(Saml2SpConfig config, String encodedArtifact)
			throws Saml2Exception {
		ArtifactResolve artifactResolve = buildArtifactResolve(config, encodedArtifact);
		Envelope envelope = buildSOAP11Envelope(artifactResolve);
		// SOAP context used by the SOAP client
		BasicSOAPMessageContext soapContext = new BasicSOAPMessageContext();
		soapContext.setOutboundMessage(envelope);
		soapContext.setSOAPRequestParameters(new HttpSOAPRequestParameters(
				"artifactResolve"));
		// Build SOAP client
		HttpClientBuilder clientBuilder = new HttpClientBuilder();
		BasicParserPool parserPool = new BasicParserPool();
		parserPool.setNamespaceAware(true);
		HttpSOAPClient soapClient = new HttpSOAPClient(
				clientBuilder.buildClient(), parserPool);
		// call ARS
		// TODO: add SSL client authn capability
		try {
			soapClient.send(config.getIdpArsServiceLocation(), soapContext);
		} catch (Exception e) {
			throw new Saml2Exception("Unable to send SOAP message", e);
		}
		// parse the response
		Envelope soapResponse = (Envelope) soapContext.getInboundMessage();
		Body responseBody = soapResponse.getBody();
		for (XMLObject xmlObject : responseBody.getUnknownXMLObjects()) {
			if (xmlObject instanceof ArtifactResponse) {
				ArtifactResponse artifactResponse = (ArtifactResponse) xmlObject;
				Status status = artifactResponse.getStatus();
				StatusCode statusCode = (status != null) ? status
						.getStatusCode() : null;
				String statusCodeStr = (statusCode != null) ? statusCode
						.getValue() : null;
				if (!StatusCode.SUCCESS_URI.equals(statusCodeStr)) {
					String statusMsg = (status.getStatusMessage() != null) ? status
							.getStatusMessage().getMessage() : null;
					throw new Saml2Exception("Invalid response status: "
							+ statusCodeStr + ", message: " + statusMsg);
				}

				SAMLObject samlObject = artifactResponse.getMessage();
				if (samlObject instanceof Response) {
					Response response = (Response) samlObject;
					return response;
				}
			}
		}
		throw new Saml2Exception("Invalid artifact response");
	}

	private ArtifactResolve buildArtifactResolve(Saml2SpConfig config, String base64Artifact)
			throws Saml2Exception {
		XMLObjectBuilderFactory bf = Configuration.getBuilderFactory();
		ArtifactResolve artifactResolve = (ArtifactResolve) bf.getBuilder(
				ArtifactResolve.DEFAULT_ELEMENT_NAME).buildObject(
				ArtifactResolve.DEFAULT_ELEMENT_NAME);

		IssuerBuilder ib = (IssuerBuilder) bf
				.getBuilder(Issuer.DEFAULT_ELEMENT_NAME);
		Issuer issuer = ib.buildObject();
		issuer.setValue(config.getSpEntityId());
		artifactResolve.setIssuer(issuer);
		artifactResolve.setIssueInstant(new DateTime());

		SecureRandomIdentifierGenerator secIdGen;
		try {
			secIdGen = new SecureRandomIdentifierGenerator();
		} catch (NoSuchAlgorithmException e) {
			throw new Saml2Exception("Unable to initialize id generator", e);
		}
		String artifactResolveId = secIdGen.generateIdentifier();
		artifactResolve.setID(artifactResolveId);
		artifactResolve.setDestination(config.getIdpArsServiceLocation());

		Artifact artifact = (Artifact) bf.getBuilder(
				Artifact.DEFAULT_ELEMENT_NAME).buildObject(
				Artifact.DEFAULT_ELEMENT_NAME);
		artifact.setArtifact(base64Artifact);
		artifactResolve.setArtifact(artifact);
		return artifactResolve;
	}

	private Envelope buildSOAP11Envelope(XMLObject payload) {
		XMLObjectBuilderFactory bf = Configuration.getBuilderFactory();
		EnvelopeBuilder eb = (EnvelopeBuilder) bf
				.getBuilder(Envelope.DEFAULT_ELEMENT_NAME);
		Envelope envelope = eb.buildObject(Envelope.DEFAULT_ELEMENT_NAME);
		BodyBuilder bb = (BodyBuilder) bf.getBuilder(Body.DEFAULT_ELEMENT_NAME);
		Body body = bb.buildObject(Body.DEFAULT_ELEMENT_NAME);
		body.getUnknownXMLObjects().add(payload);
		envelope.setBody(body);
		return envelope;
	}

	private Assertion validateAuthnResponse(Saml2SpConfig config, Response authnResponse)
			throws Saml2Exception {

		if (config.isSpAcsWantsResponseSigned()) {
			if (!authnResponse.isSigned()) {
				throw new Saml2Exception("Authn response is not signed");
			}
			try {
				SignatureValidator idpSignatureValidator = 
						new SignatureValidator(config.getIdpSignatureValidationCredential());
				idpSignatureValidator.validate(authnResponse.getSignature());
			} catch (ValidationException e) {
				throw new Saml2Exception(
						"Error validating response signature", e);
			}
		}
		
		Assertion assertion;
		// Get the list of assertions
		List<Assertion> assertionsList = authnResponse.getAssertions();
		// Make sure at least one is present
		if (assertionsList.size() == 0) {
			throw new Saml2Exception("No assertion found in SAML response");
		}
		// Get the first one only
		assertion = (Assertion) assertionsList.get(0);
		
		// assertion signature validation
		if (config.isSpAcsWantsAssertionSigned()) {
			if (!assertion.isSigned()) {
				throw new Saml2Exception("Assertion is not signed");
			}
			try {
				SignatureValidator idpSignatureValidator = 
						new SignatureValidator(config.getIdpSignatureValidationCredential());
				idpSignatureValidator.validate(assertion.getSignature());
			} catch (ValidationException e) {
				throw new Saml2Exception(
						"Error validating assertion signature", e);
			}
		}
		checkConditions(config, assertion);
		checkSubject(config, assertion);
		
		return assertion;
	}

	private void checkSubject(Saml2SpConfig config, Assertion assertion) throws Saml2Exception {
		Subject samlSubject = assertion.getSubject();
		if (samlSubject == null) {
			throw new Saml2Exception("No subject found in SAML assertion");
		}
		NameID nameId = samlSubject.getNameID();
		String prefferedNameIdFormat = config.getSpNameIdFormat();
		if (nameId.getFormat() != null
				&& !prefferedNameIdFormat.equals(nameId.getFormat())) {
			throw new Saml2Exception("Unsupported name id format "
					+ nameId.getFormat());
		}
		
		List<SubjectConfirmation> subjectConfirmationList = samlSubject
				.getSubjectConfirmations();
		String spAcsLocation = config.getSpAcsLocation();
		for (SubjectConfirmation subjectConfirmation : subjectConfirmationList) {
			if (Saml2SpConfig.SAML_CONFIRMATION_BEARER
					.equals(subjectConfirmation.getMethod())) {
				SubjectConfirmationData confData = subjectConfirmation
						.getSubjectConfirmationData();
				if (confData == null) {
					log.debug("Subject confirmation data is missing");
					continue;
				}

				long currentTime = System.currentTimeMillis();
				DateTime notOnOrAfter = confData.getNotOnOrAfter();
				if (notOnOrAfter != null) {
					if (notOnOrAfter.isBefore(currentTime
							- config.getSpMaxClockSkew()
							* MILLIS_PER_MINUTE)) {
						log.debug("NotOnOrAfter date check failed");
						continue;
					}
				}
				DateTime notBefore = confData.getNotBefore();
				if (notBefore != null) {
					if (notBefore.isAfter(currentTime
							+ config.getSpMaxClockSkew()
							* MILLIS_PER_MINUTE)) {
						log.debug("NotOnOrAfter date check failed");
						continue;
					}
				}

				if (!spAcsLocation.equals(confData.getRecipient())) {
					log.debug(
							"Subject confirmation recipient doesn't match to ACS url. Expected: " 
									+ spAcsLocation + ", actual:" + confData.getRecipient());
					continue;
				}

				return;
			}
		}
		throw new Saml2Exception("No valid Subject confirmation found in assertion");
	}

	private void checkConditions(Saml2SpConfig config, Assertion assertion) throws Saml2Exception {
		Conditions conditions = assertion.getConditions();
		if (conditions != null) {
			long currentMillis = System.currentTimeMillis();
			long maxClockSkew = config.getSpMaxClockSkew();
			if (conditions.getNotBefore() != null) {
				if (conditions.getNotBefore().isAfter(
						currentMillis + maxClockSkew * MILLIS_PER_MINUTE)) {
					throw new Saml2Exception(
							"Not before condition validation error");
				}
			}
			if (conditions.getNotOnOrAfter() != null) {
				if (conditions.getNotOnOrAfter().isBefore(
						currentMillis - maxClockSkew * MILLIS_PER_MINUTE)) {
					throw new Saml2Exception(
							"Not after condition validation error");
				}
			}
			List<AudienceRestriction> audienceRestrictionList = conditions
					.getAudienceRestrictions();
			if (audienceRestrictionList != null) {
				for (AudienceRestriction audienceRestriction : audienceRestrictionList) {
					if (audienceRestriction.getAudiences() != null) {
						boolean matched = false;
						String spEntityId = config.getSpEntityId();
						for (Audience audience : audienceRestriction
								.getAudiences()) {
							String audienceUri = audience.getAudienceURI();
							if (audienceUri != null
									&& spEntityId.equals(audienceUri.trim())) {
								matched = true;
								break;
							}
						}
						if (!matched) {
							throw new Saml2Exception(
									"SP EntityID is not found in audience restrictions");
						}
					}
				}
			}
		}
	}

	public String getSubjectName(Assertion assertion) throws Saml2Exception {
		Subject samlSubject = assertion.getSubject();
		NameID nameId = samlSubject.getNameID();
		if (nameId == null) {
			throw new Saml2Exception(
					"No subject nameId found in SAML assertion");
		}

		String username = nameId.getValue();
		if (username == null || username.trim().isEmpty()) {
			throw new Saml2Exception("Empty subject name id ");
		}
		return username;
	}

	public List<String> getSessionIndices(Assertion assertion) {
		ArrayList<String> sessionIndexList = new ArrayList<String>();
		for (AuthnStatement authnStmt: assertion.getAuthnStatements()) {
			String sessionIndex = authnStmt.getSessionIndex();
			if (sessionIndex != null) {
				sessionIndexList.add(sessionIndex);
			}
		}
		return sessionIndexList;
	}
	
	public void sendLogoutRequest(Saml2SpConfig config, String subjectId, List<String> sessionIndices,
			HttpServletRequest request, HttpServletResponse response) throws Saml2Exception {
		LogoutRequest logoutRequest;
		try {
			logoutRequest = buildLogoutRequest(config, subjectId, sessionIndices);
		} catch (Exception e) {
			throw new Saml2Exception("Unable to build Logout Request object", e);
		}

		Endpoint endpoint = buildSloRequestEndpoint(
				config.getIdpSsoServiceBinding(),
				config.getIdpSloServiceRequestLocation());
		if (SAMLConstants.SAML2_POST_BINDING_URI.equals(config
				.getIdpSsoServiceBinding())) {
			try {
				sendPostMessage(logoutRequest, null, endpoint, response);
			} catch (Exception e) {
				throw new Saml2Exception("Unable to send POST Logout Request", e);
			}
		} else if (SAMLConstants.SAML2_REDIRECT_BINDING_URI.equals(config
				.getIdpSsoServiceBinding())) {
			try {
				sendRedirectMessage(logoutRequest, null, endpoint, response);
			} catch (Exception e) {
				throw new Saml2Exception("Unable to send REDIRECT Logout Request", e);
			}
		} else {
			throw new Saml2Exception("Unsupported binding of IdP SLO service: "
					+ config.getIdpSsoServiceBinding());
		}
	}

	private LogoutRequest buildLogoutRequest(Saml2SpConfig config, String subjectId, List<String> sessionIndices)
			throws NoSuchAlgorithmException, Saml2Exception, SecurityException,
			SignatureException {

		XMLObjectBuilderFactory bf = Configuration.getBuilderFactory();
		LogoutRequest logoutRequest = (LogoutRequest) bf.getBuilder(
				LogoutRequest.DEFAULT_ELEMENT_NAME).buildObject(
				LogoutRequest.DEFAULT_ELEMENT_NAME);

		IssuerBuilder ib = (IssuerBuilder) bf
				.getBuilder(Issuer.DEFAULT_ELEMENT_NAME);
		Issuer issuer = ib.buildObject();
		issuer.setValue(config.getSpEntityId());
		logoutRequest.setIssuer(issuer);
		logoutRequest.setIssueInstant(new DateTime());
		SecureRandomIdentifierGenerator secIdGen = new SecureRandomIdentifierGenerator();
		String requestId = secIdGen.generateIdentifier();
		logoutRequest.setID(requestId);
		logoutRequest.setDestination(config.getIdpSloServiceRequestLocation());
		NameID nameId = (NameID) bf.getBuilder(NameID.DEFAULT_ELEMENT_NAME)
				.buildObject(NameID.DEFAULT_ELEMENT_NAME);
		nameId.setFormat(config.getSpNameIdFormat());
		nameId.setValue(subjectId);
		logoutRequest.setNameID(nameId);
		if (sessionIndices != null) {
			for (String sessionIdx: sessionIndices) {
				SessionIndex sessionIndex = (SessionIndex) bf.getBuilder(
						SessionIndex.DEFAULT_ELEMENT_NAME).buildObject(
						SessionIndex.DEFAULT_ELEMENT_NAME);
				sessionIndex.setSessionIndex(sessionIdx);
				logoutRequest.getSessionIndexes().add(sessionIndex);
			}
		}

		if (config.isIdpSloWantsRequestsSigned()) {
			Credential spSingingCredential = config.getSpSigningCredential();
			if (spSingingCredential == null) {
				throw new Saml2Exception("No SP singning credetial defined");
			}
			Signature signature = (Signature) bf
					.getBuilder(Signature.DEFAULT_ELEMENT_NAME);
			logoutRequest.setSignature(signature);
			SecurityHelper.prepareSignatureParams(signature, spSingingCredential, null, null);
			Signer.signObject(signature);
		}

		return logoutRequest;
	}

	public void processSloRedirect(Saml2SpConfig config, HttpServletRequest request,
			HttpServletResponse response) throws Saml2Exception {
		HTTPRedirectDeflateDecoder decoder = new HTTPRedirectDeflateDecoder();
		HttpServletRequestAdapter adapter = new HttpServletRequestAdapter(
				request);
		BasicSAMLMessageContext context = new BasicSAMLMessageContext();
		context.setInboundMessageTransport(adapter);
		
		try {
			decoder.decode(context);
		} catch (Exception e) {
			throw new Saml2Exception("Unable to decode redirect message", e);
		}

		XMLObject xmlObj = context.getInboundMessage();
		if (xmlObj instanceof LogoutRequest) {
			LogoutRequest logoutRequest = (LogoutRequest) xmlObj;
			processSloRequest(config, logoutRequest, request, response);
		} else if (xmlObj instanceof LogoutResponse) {
			LogoutResponse logoutResponse = (LogoutResponse) xmlObj;
			processSloResponse(config, logoutResponse, request, response);
		} else {
			throw new Saml2Exception("Invalid message");
		}
	}

	public void processSloPost(Saml2SpConfig config, HttpServletRequest request,
			HttpServletResponse response) throws MessageDecodingException,
			SecurityException, Saml2Exception, MessageEncodingException,
			NoSuchAlgorithmException, BindingException, IOException,
			MarshallingException, UnsupportedCallbackException,
			SignatureException {
		HTTPPostDecoder decoder = new HTTPPostDecoder();
		HttpServletRequestAdapter adapter = new HttpServletRequestAdapter(
				request);
		BasicSAMLMessageContext context = new BasicSAMLMessageContext();
		context.setInboundMessageTransport(adapter);
		decoder.decode(context);

		XMLObject xmlObj = context.getInboundMessage();
		if (xmlObj instanceof LogoutRequest) {
			LogoutRequest logoutRequest = (LogoutRequest) xmlObj;
			processSloRequest(config, logoutRequest, request, response);
		} else if (xmlObj instanceof LogoutResponse) {
			LogoutResponse logoutResponse = (LogoutResponse) xmlObj;
			processSloResponse(config, logoutResponse, request, response);
		} else {
			throw new Saml2Exception("Invalid message");
		}
	}

	private void processSloResponse(Saml2SpConfig config, LogoutResponse logoutResponse,
			HttpServletRequest request, HttpServletResponse response)
			throws Saml2Exception {
		// validate signature
		if (config.isSpWantsLogoutSigned()) {
			if (!logoutResponse.isSigned()) {
				throw new Saml2Exception("LogoutResponse is not signed");
			}
			try {
				Credential idpSigningCert = config.getIdpSignatureValidationCredential();
				SignatureValidator idpSignatureValidator = new SignatureValidator(idpSigningCert);
				idpSignatureValidator.validate(logoutResponse.getSignature());
			} catch (ValidationException e) {
				throw new Saml2Exception("Error validating signature", e);
			}
		}
		// validate issuer
		Issuer issuer = logoutResponse.getIssuer();
		if (issuer == null
				|| !config.getIdpEntityId().equals(issuer.getValue())) {
			throw new Saml2Exception(
					"Invalid response issuer: " + issuer != null ? issuer
							.getValue() : "null");
		}

		HttpSession session = request.getSession(false);
		if (session != null) {
			session.invalidate();
		}

		String logoutCompletedUrl = config.getSpLogoutCompletedUrl();
		if (StringUtils.isBlank(logoutCompletedUrl)) {
			logoutCompletedUrl = config.getSpBaseUrl();
		}

		try {
			response.sendRedirect(logoutCompletedUrl);
		} catch (IOException e) {
			throw new Saml2Exception("Unable to redirect to logout URL", e);
		}
	}

	private void processSloRequest(Saml2SpConfig config, LogoutRequest logoutRequest,
			HttpServletRequest request, HttpServletResponse response)
			throws Saml2Exception {
		// validate signature
		if (config.isSpWantsLogoutSigned()) {
			if (!logoutRequest.isSigned()) {
				throw new Saml2Exception("LogoutRequest is not signed");
			}
			try {
				Credential idpSigningCert = config.getIdpSignatureValidationCredential();
				SignatureValidator idpSignatureValidator = new SignatureValidator(
						idpSigningCert);
				idpSignatureValidator.validate(logoutRequest.getSignature());
			} catch (ValidationException e) {
				throw new Saml2Exception("Error validating signature", e);
			}
		}
		// validate issuer
		Issuer issuer = logoutRequest.getIssuer();
		if (issuer == null
				|| !config.getIdpEntityId().equals(issuer.getValue())) {
			throw new Saml2Exception(
					"Invalid request issuer: " + issuer != null ? issuer
							.getValue() : "null");
		}

		HttpSession session = request.getSession(false);
		if (session != null) {
			session.invalidate();
		}

		sendLogoutResponse(config, logoutRequest, request, response);
	}

	private void sendLogoutResponse(Saml2SpConfig config, LogoutRequest logoutRequest,
			HttpServletRequest request, HttpServletResponse response)
			throws Saml2Exception {
		LogoutResponse logoutResponse = buildLogoutResponse(config, logoutRequest);

		Endpoint endpoint = buildSloRequestEndpoint(
				config.getIdpSsoServiceBinding(),
				config.getIdpSloServiceResponseLocation());
		if (SAMLConstants.SAML2_POST_BINDING_URI.equals(config
				.getIdpSsoServiceBinding())) {
			try {
				sendPostMessage(logoutResponse, null, endpoint, response);
			} catch (Exception e) {
				throw new Saml2Exception("Unable to send POST Logout Response", e);
			}
		} else if (SAMLConstants.SAML2_REDIRECT_BINDING_URI.equals(config
				.getIdpSsoServiceBinding())) {
			try {
				sendRedirectMessage(logoutResponse, null, endpoint, response);
			} catch (Exception e) {
				throw new Saml2Exception("Unable to send REDIRECT Logout Response", e);
			}
		} else {
			throw new Saml2Exception("Unsupported binding of IdP SLO service: "
					+ config.getIdpSsoServiceBinding());
		}
	}

	private Endpoint buildSloRequestEndpoint(String binding, String location) {
		XMLObjectBuilderFactory builderFactory = Configuration
				.getBuilderFactory();
		SingleLogoutServiceBuilder sloBuilder = (SingleLogoutServiceBuilder) builderFactory
				.getBuilder(SingleLogoutService.DEFAULT_ELEMENT_NAME);
		SingleLogoutService slo = sloBuilder.buildObject();
		slo.setBinding(binding);
		slo.setLocation(location);
		return slo;
	}

	private LogoutResponse buildLogoutResponse(Saml2SpConfig config, LogoutRequest logoutRequest)
			throws Saml2Exception {
		XMLObjectBuilderFactory bf = Configuration.getBuilderFactory();
		LogoutResponse logoutResponse = (LogoutResponse) bf.getBuilder(
				LogoutResponse.DEFAULT_ELEMENT_NAME).buildObject(
				LogoutResponse.DEFAULT_ELEMENT_NAME);

		IssuerBuilder ib = (IssuerBuilder) bf
				.getBuilder(Issuer.DEFAULT_ELEMENT_NAME);
		Issuer issuer = ib.buildObject();
		issuer.setValue(config.getSpEntityId());
		logoutResponse.setIssuer(issuer);
		logoutResponse.setIssueInstant(new DateTime());
		SecureRandomIdentifierGenerator secIdGen;
		try {
			secIdGen = new SecureRandomIdentifierGenerator();
		} catch (NoSuchAlgorithmException e) {
			throw new Saml2Exception("Unable to initialize id generator", e);
		}
		String responseId = secIdGen.generateIdentifier();
		logoutResponse.setID(responseId);
		logoutResponse
				.setDestination(config.getIdpSloServiceResponseLocation());
		logoutResponse.setInResponseTo(logoutRequest.getID());

		if (config.isIdpSloWantsRequestsSigned()) {
			Credential spSigningCredential = config.getIdpSignatureValidationCredential();
			if (spSigningCredential == null) {
				throw new Saml2Exception("No Sp signing key defined");
			}
			Signature signature = (Signature) bf
					.getBuilder(Signature.DEFAULT_ELEMENT_NAME);
			logoutResponse.setSignature(signature);
			try {
				SecurityHelper.prepareSignatureParams(signature, spSigningCredential, null, null);
				Signer.signObject(signature);
			} catch (Exception e) {
				throw new Saml2Exception("Unable to sign Logout Response", e);
			}
		}

		return logoutResponse;
	}

	private void sendRedirectMessage(SAMLObject message, String relayState, Endpoint endpoint,
			HttpServletResponse response)
			throws org.opensaml.xml.io.MarshallingException, BindingException,
			IOException, MessageEncodingException, NoSuchAlgorithmException,
			UnsupportedCallbackException, Saml2Exception, SignatureException,
			SecurityException {
		HttpServletResponseAdapter adapter = new HttpServletResponseAdapter(
				response, false);
		BasicSAMLMessageContext context = new BasicSAMLMessageContext();
		context.setOutboundMessageTransport(adapter);
		context.setOutboundSAMLMessage(message);
		context.setPeerEntityEndpoint(endpoint);
		context.setRelayState(relayState);
		HTTPRedirectDeflateEncoder encoder = new HTTPRedirectDeflateEncoder();
		encoder.encode(context);
	}

	private void sendPostMessage(SAMLObject message, String relayState, Endpoint endpoint,
			HttpServletResponse response) throws IOException,
			MarshallingException, MessageEncodingException,
			NoSuchAlgorithmException, UnsupportedCallbackException,
			Saml2Exception, SignatureException, SecurityException {

		VelocityEngine velocityEngine = new VelocityEngine();
		String templateId = "";

		HTTPPostEncoder encoder = new HTTPPostEncoder(velocityEngine,
				templateId);
		HttpServletResponseAdapter adapter = new HttpServletResponseAdapter(
				response, false);
		BasicSAMLMessageContext context = new BasicSAMLMessageContext();
		context.setOutboundMessageTransport(adapter);
		context.setOutboundSAMLMessage(message);
		context.setPeerEntityEndpoint(endpoint);
		context.setRelayState(relayState);
		encoder.encode(context);
	}

	public void sendMetadata(Saml2SpConfig config, HttpServletRequest request,
			HttpServletResponse response) throws Saml2Exception {

		EntityDescriptor spEntityDescriptor = buildSPEntitydescriptor(config);

		try {
			DocumentBuilder builder;
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	
			builder = factory.newDocumentBuilder();
			Document document = builder.newDocument();
			Marshaller out = Configuration.getMarshallerFactory().getMarshaller(
					spEntityDescriptor);
			out.marshall(spEntityDescriptor, document);
	
			Transformer transformer = TransformerFactory.newInstance()
					.newTransformer();
			StreamResult streamResult = new StreamResult(response.getOutputStream());
			DOMSource source = new DOMSource(document);
			transformer.transform(source, streamResult);
		} catch (Exception e) {
			throw new Saml2Exception("Unable to send metadata", e);
		}
		
		response.setHeader("Cache-control", "no-cache, no-store");
		response.setHeader("Pragma", "no-cache");
		response.setCharacterEncoding("UTF-8");
		response.setHeader("Content-Type", "text/xml");
	}

	private EntityDescriptor buildSPEntitydescriptor(Saml2SpConfig config) throws Saml2Exception {

		XMLObjectBuilderFactory builderFactory = Configuration
				.getBuilderFactory();
		EntityDescriptorBuilder edb = (EntityDescriptorBuilder) builderFactory
				.getBuilder(EntityDescriptor.DEFAULT_ELEMENT_NAME);
		EntityDescriptor spEntityDescriptor = edb.buildObject();
		spEntityDescriptor.setEntityID(config.getSpEntityId());

		SPSSODescriptorBuilder spdb = (SPSSODescriptorBuilder) builderFactory
				.getBuilder(SPSSODescriptor.DEFAULT_ELEMENT_NAME);
		SPSSODescriptor spSSODescriptor = spdb.buildObject();

		spSSODescriptor.setWantAssertionsSigned(config
				.isSpAcsWantsAssertionSigned());
		spSSODescriptor.setAuthnRequestsSigned(config.isIdpSsoWantsRequestsSigned());

		Credential spSignCredential = config.getSpSigningCredential();
		if (spSignCredential != null) {
			X509KeyInfoGeneratorFactory keyInfoGeneratorFactory = new X509KeyInfoGeneratorFactory();
			keyInfoGeneratorFactory.setEmitEntityCertificate(true);
			KeyInfoGenerator keyInfoGenerator = keyInfoGeneratorFactory
					.newInstance();
			KeyDescriptorBuilder kdb = (KeyDescriptorBuilder) builderFactory
					.getBuilder(KeyDescriptor.DEFAULT_ELEMENT_NAME);
			KeyDescriptor signKeyDescriptor = kdb.buildObject();
			signKeyDescriptor.setUse(UsageType.SIGNING);
			try {
				signKeyDescriptor.setKeyInfo(keyInfoGenerator.generate(spSignCredential));
			} catch (SecurityException e) {
				log.error("Error getting signing credential", e);
			}
			if (signKeyDescriptor.getKeyInfo() != null) {
				spSSODescriptor.getKeyDescriptors().add(signKeyDescriptor);
			}
		}

		// NameIDFormat
		NameIDFormatBuilder nifb = (NameIDFormatBuilder) builderFactory
				.getBuilder(NameIDFormat.DEFAULT_ELEMENT_NAME);
		NameIDFormat nameIDFormat = nifb.buildObject();
		nameIDFormat.setFormat(config.getSpNameIdFormat());
		spSSODescriptor.getNameIDFormats().add(nameIDFormat);

		// ACS endpoint
		AssertionConsumerServiceBuilder acsb = (AssertionConsumerServiceBuilder) builderFactory
				.getBuilder(AssertionConsumerService.DEFAULT_ELEMENT_NAME);
		AssertionConsumerService assertionConsumerService = acsb.buildObject();
		assertionConsumerService.setIndex(0);
		assertionConsumerService.setBinding(config.getSpAcsBinding());
		String acsLocation = config.getSpAcsLocation();
		assertionConsumerService.setLocation(acsLocation);
		assertionConsumerService.setIsDefault(true);
		spSSODescriptor.getAssertionConsumerServices().add(
				assertionConsumerService);
		// SLO endpoint
		if (!StringUtils.isBlank(config.getSpSloBinding())) {
			SingleLogoutServiceBuilder slosb = (SingleLogoutServiceBuilder) builderFactory
					.getBuilder(SingleLogoutService.DEFAULT_ELEMENT_NAME);
			SingleLogoutService singleLogoutService = slosb.buildObject();
			singleLogoutService.setBinding(config.getSpSloBinding());
			singleLogoutService.setLocation(config.getSpSloLocation());
			singleLogoutService.setResponseLocation(config.getSpSloResponseLocation());
			spSSODescriptor.getSingleLogoutServices().add(singleLogoutService);
		}
		// supported protocols
		spSSODescriptor.addSupportedProtocol(SAMLConstants.SAML20P_NS);
		spEntityDescriptor.getRoleDescriptors().add(spSSODescriptor);

		return spEntityDescriptor;
	}

	public String getFirstSAMLAttributeValue(Assertion assertion, String attrName) {
		List<String> values = getSAMLAttributeValues(assertion, attrName);
		if (values.isEmpty()) {
			return null;
		}
		return values.get(0);
	}
	
	public List<String> getSAMLAttributeValues(Assertion assertion, String attrName) {
		List<String> values = new ArrayList<String>();
		if (StringUtils.isBlank(attrName)) {
			return new ArrayList<String>();
		}
		List<AttributeStatement> attrStmtList = assertion.getAttributeStatements();
		for (AttributeStatement attrStmt : attrStmtList) {
			List<Attribute> attrList = attrStmt.getAttributes();
			for (Attribute attr: attrList) {
				if (attrName.equalsIgnoreCase(attr.getName())) {
					List<XMLObject> attrValues = attr.getAttributeValues();
					attr.getAttributeValues();
					for (XMLObject xmlObject: attrValues) {
						Element element = xmlObject.getDOM();
						String value = element.getTextContent();
						if (!StringUtils.isBlank(value)) {
							values.add(value.trim());
						}
					}
					break;
				}
			}
		}
		return values;
	}
	
	
}
