/*
 * (C) Copyright 2013 drodak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package org.bezkres.saml2.util;

import org.opensaml.xml.security.credential.Credential;

public interface Saml2SpConfig {

	public static final String SAML_NAMEID_FORMAT_UNSPECIFIED = "urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified";
	public static final String SAML_CONFIRMATION_BEARER = "urn:oasis:names:tc:SAML:2.0:cm:bearer";
	
	
	public String getSpAcsLocation();

	public String getSpSloLocation();

	public String getSpSloResponseLocation();

	public String getSpEntityId();

	public String getSpBaseUrl();

	public boolean isSpForceReAuthentication();

	public String getSpAcsBinding();

	public boolean isSpAcsWantsAssertionSigned();

	public String getSpSloBinding();

	public boolean isSpWantsLogoutSigned();

	public String getSpNameIdFormat();

	public String getSpLogoutCompletedUrl();

	public Credential getSpSigningCredential();

	public long getSpMaxClockSkew();

	public String getIdpEntityId();

	public String getIdpSsoServiceLocation();

	public String getIdpSsoServiceBinding();

	public boolean isIdpSsoWantsRequestsSigned();

	public String getIdpArsServiceLocation();

	public String getIdpSloServiceBinding();

	public String getIdpSloServiceRequestLocation();

	public String getIdpSloServiceResponseLocation();

	public abstract boolean isIdpSloWantsRequestsSigned();

	public abstract Credential getIdpSignatureValidationCredential();

	public boolean isSpAcsWantsResponseSigned();

	public Integer getSpAttributeConsumingServiceIndex();

}