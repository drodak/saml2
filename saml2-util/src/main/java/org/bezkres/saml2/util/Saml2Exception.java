/*
 * (C) Copyright 2013 drodak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package org.bezkres.saml2.util;



public class Saml2Exception extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7357786124920760714L;

	public Saml2Exception() {
		super();
	}

	public Saml2Exception(String message, Throwable cause) {
		super(message, cause);
	}

	public Saml2Exception(String message) {
		super(message);
	}

	public Saml2Exception(Throwable cause) {
		super(cause);
	}
	
}
