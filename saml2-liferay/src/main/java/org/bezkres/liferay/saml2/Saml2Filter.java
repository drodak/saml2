/*
 * (C) Copyright 2013 drodak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package org.bezkres.liferay.saml2;

import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.bezkres.saml2.util.Saml2Exception;
import org.bezkres.saml2.util.Saml2Util;
import org.opensaml.common.xml.SAMLConstants;
import org.opensaml.saml2.core.Assertion;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.BaseFilter;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.PrefsPropsUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.util.PortalUtil;

public class Saml2Filter extends BaseFilter {

	private static Log log = LogFactoryUtil.getLog(Saml2Filter.class);

	protected Log getLog() {
		return log;
	}

	@Override
	public boolean isFilterEnabled(HttpServletRequest request,
			HttpServletResponse response) {
		
		try {
			long companyId = PortalUtil.getCompanyId(request);

			boolean def = GetterUtil.getBoolean(PropsUtil.get(LiferaySaml2SpConfig.PREF_SP_ENABLED));
			boolean enabled = PrefsPropsUtil.getBoolean(companyId, LiferaySaml2SpConfig.PREF_SP_ENABLED, def);
			
			return enabled;
		} catch (Exception e) {
			log.error("Unable to get preferences", e);
			return false;
		}
	}
	
	@Override
	protected void processFilter(HttpServletRequest request, HttpServletResponse response,
			FilterChain filterChain) throws Exception {

        String pathInfo = request.getPathInfo();
        
        Saml2Util saml2Util = Saml2Util.getInstance();
        LiferaySaml2SpConfig config = new LiferaySaml2SpConfig(request);
        
        if (pathInfo.indexOf("/portal/login") != -1) {
        	saml2Util.sendAuthnRequest(config, request, response, null);
        	return;
        }
        
        if (pathInfo.indexOf("/portal/logout") != -1) {
        	HttpSession session = request.getSession(false);
        	Assertion assertion = null;
        	if (session != null) {
        		assertion = (Assertion) session.getAttribute(LiferaySaml2SpConfig.ATTR_ASSERTION);
        		session.invalidate();
        	}
        	if (assertion != null) {
            	// initiate single logout profile
        		String subjectId = saml2Util.getSubjectName(assertion);
        		List<String> sessionIndices = saml2Util.getSessionIndices(assertion);
        		saml2Util.sendLogoutRequest(config, subjectId, sessionIndices, request, response);
        	} else {
        		// redirect to SP' home page
        		response.sendRedirect(config.getSpBaseUrl());
        	}
        	return;
        }

		if (LiferaySaml2SpConfig.SP_ACS_URI.indexOf(pathInfo) != -1) {
        	Assertion assertion;
        	if (SAMLConstants.SAML2_ARTIFACT_BINDING_URI.equals(config.getSpAcsBinding())) {
        		try {
					assertion = saml2Util.processAcsArtifact(config, request, response);
				} catch (Exception e) {
					throw new ServletException("Unable to process ACS request", e);
				}
        	} else if (SAMLConstants.SAML2_POST_BINDING_URI.equals(config.getSpAcsBinding())) {
        		try {
					assertion = saml2Util.processAcsPost(config, request, response);
				} catch (Saml2Exception e) {
					throw new ServletException("Unable to process ACS POST request", e);
				}
        	} else {
        		throw new ServletException("Unsupported ACS binding: " + config.getSpAcsBinding());
        	}
        	HttpSession session = request.getSession(true);
        	session.setAttribute(LiferaySaml2SpConfig.ATTR_AUTOLOGIN_TOKEN, assertion);
    		String redirectUrl = config.getSpBaseUrl();
    		response.sendRedirect(redirectUrl);
        	return;
        }
        
        if (LiferaySaml2SpConfig.SP_SLO_URI.indexOf(pathInfo) != -1) {
        	if (SAMLConstants.SAML2_REDIRECT_BINDING_URI.equals(config.getSpSloBinding())) {
        		try {
					saml2Util.processSloRedirect(config, request, response);
				} catch (Saml2Exception e) {
					throw new ServletException("Unable to process Logout request", e);
				}
        	} else if (SAMLConstants.SAML2_POST_BINDING_URI.equals(config.getSpSloBinding())) {
        		try {
					saml2Util.processSloPost(config, request, response);
				} catch (Exception e) {
					throw new ServletException("Unable to process POST Logout request", e);
				}
        	} else {
        		throw new ServletException("Unsupported SLO binding: " + config.getSpSloBinding());
        	}
        	return;
        }
        
        if (LiferaySaml2SpConfig.SP_METADATA_URI.indexOf(pathInfo) != -1) {
        	if (config.isPublishMetadata()) {
        		try {
					saml2Util.sendMetadata(config, request, response);
				} catch (Saml2Exception e) {
					throw new ServletException("Unable to process metadata request", e);
				}
        		return;
        	} 
        }
        
		super.processFilter(request, response, filterChain);
	}
}
