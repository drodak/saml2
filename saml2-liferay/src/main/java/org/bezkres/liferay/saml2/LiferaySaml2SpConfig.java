/*
 * (C) Copyright 2013 drodak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package org.bezkres.liferay.saml2;

import java.io.FileInputStream;
import java.security.KeyStore;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.bezkres.saml2.util.Saml2SpConfig;
import org.opensaml.common.xml.SAMLConstants;
import org.opensaml.xml.security.credential.Credential;
import org.opensaml.xml.security.x509.KeyStoreX509CredentialAdapter;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.PrefsPropsUtil;
import com.liferay.portal.util.PortalUtil;

public class LiferaySaml2SpConfig implements Saml2SpConfig {

	static final String ATTR_AUTOLOGIN_TOKEN = "SAML2_AUTOLOGIN_TOKEN";
	static final String ATTR_ASSERTION = "SAML2_ASSERTION";
	
	
	static final String SP_ACS_URI = "/c/portal/saml2/acs";
	static final String SP_SLO_URI = "/c/portal/saml2/slo";
	static final String SP_METADATA_URI = "/c/portal/saml2/metadata";
	
	public static final String PREF_KEYSTORE_FILE = "saml2.sp.keystoreFile";
	public static final String PREF_KEYSTORE_PASSWD = "saml2.sp.keystorePasswd";
	public static final String PREF_TRUSTSTORE_FILE = "saml2.sp.trustStoreFile";
	public static final String PREF_IMPORT_USER = "saml2.sp.importUser";
	public static final String PREF_UPDATE_USER = "saml2.sp.updateUser";
	
	public static final String PREF_SP_ENABLED = "saml2.sp.enabled";
	public static final String PREF_SP_ENTITYID = "saml2.sp.entityId";
	public static final String PREF_SP_FRONTEND_URL = "saml2.sp.frontendUrl";
	public static final String PREF_SP_FORCE_REAUTHENTICATION = "saml2.sp.forceReauthentication";
	public static final String PREF_SP_ACS_BINDING = "saml2.sp.acsBinding";
	public static final String PREF_SP_ACS_WANTS_ASSERTION_SIGNED = "saml2.sp.acsWantsAssertionSigned";
	public static final String PREF_SP_ACS_WANTS_RESPONSE_SIGNED = "saml2.sp.acsWantsResponseSigned";
	public static final String PREF_SP_SLO_BINDING = "saml2.sp.sloBinding";
	public static final String PREF_SP_SLO_WANTS_REQUEST_SIGNED = "saml2.sp.sloWantsRequestSigned";
	public static final String PREF_SP_NAME_ID_FORMAT = "saml2.sp.nameIdFormat";
	public static final String PREF_SP_LOGOUT_COMPLETED_URL = "saml2.sp.logoutCompletedUrl";
	public static final String PREF_SP_SIGN_KEY_ALIAS = "saml2.sp.signKeyAlias";
	public static final String PREF_SP_SIGN_KEY_PASSWD = "saml2.sp.signKeyPasswd";
	public static final String PREF_SP_MAX_CLOCK_SKEW = "saml2.sp.maxClockSkew";
	public static final String PREF_SP_PUBLISH_METADATA = "saml2.sp.publishMetadata";

	public static final String PREF_SP_ATTRIBUTE_CONSUMING_SERVICE_INDEX = "saml2.sp.attributeConsumingServiceIndex";
	public static final String PREF_SP_SAMLATTR_ROLES = "saml2.sp.rolesAttribute";
	public static final String PREF_SP_SAMLATTR_ROLES_COMA_SEPARATED = "saml2.sp.rolesAttributeComaSeparated";
	public static final String PREF_SP_SAMLATTR_GROUPS = "saml2.sp.groupsAttribute";
	public static final String PREF_SP_SAMLATTR_GROUPS_COMA_SEPARATED = "saml2.sp.groupsAttributeComaSeparated";
	public static final String PREF_SP_SAMLATTR_FIRSTNAME = "saml2.sp.firstNameAttribute";
	public static final String PREF_SP_SAMLATTR_LASTNAME = "saml2.sp.lastNameAttribute";
	public static final String PREF_SP_SAMLATTR_EMAIL = "saml2.sp.emailAttribute";

	public static final String PREF_IDP_ENTITY_ID = "saml2.idp.entityId";
	public static final String PREF_IDP_SSO_LOCATION = "saml2.idp.ssoLocation";
	public static final String PREF_IDP_SSO_BINDING = "saml2.idp.ssoBinding";
	public static final String PREF_IDP_SSO_WANTS_REQUEST_SIGNED = "saml2.idp.ssoWantsRequestSigned";
	public static final String PREF_IDP_ARS_LOCATION = "saml2.idp.arsLocation";
	public static final String PREF_IDP_SLO_BINDING = "saml2.idp.sloBinding";
	public static final String PREF_IDP_SLO_REQUEST_LOCATION = "saml2.idp.sloRequestLocation";
	public static final String PREF_IDP_SLO_RESPONSE_LOCATION = "saml2.idp.sloResponseLocation";
	public static final String PREF_IDP_SLO_WANTS_REQUEST_SIGNED = "saml2.idp.sloWantsRequestSigned";
	public static final String PREF_IDP_CERT_ALIAS = "saml2.idp.certAlias";
	

	private static Log log = LogFactoryUtil.getLog(LiferaySaml2SpConfig.class);

	private long companyId;
	private String baseUrl;
	
	public LiferaySaml2SpConfig(HttpServletRequest request) {
		companyId = PortalUtil.getCompanyId(request);
		baseUrl = getString(PREF_SP_FRONTEND_URL);
		if (StringUtils.isBlank(baseUrl)) {
			baseUrl = getRequestUrl(request);
		}
	}

	protected String getString(String key) {
		try {
			String def = PrefsPropsUtil.getString(key);
			return PrefsPropsUtil.getString(companyId, key, def);
		} catch (Exception e) {
			throw new RuntimeException("Unable to read preferences", e);
		}
	}

	protected String getString(String key, String defaultValue) {
		try {
			String def = PrefsPropsUtil.getString(key, defaultValue);
			return PrefsPropsUtil.getString(companyId, key, def);
		} catch (Exception e) {
			throw new RuntimeException("Unable to read preferences", e);
		}
	}

	protected boolean getBoolean(String key, boolean defaultValue) {
		try {
			boolean def = GetterUtil.get(PrefsPropsUtil.getString(key), defaultValue);
			return PrefsPropsUtil.getBoolean(companyId, key, def);
		} catch (Exception e) {
			throw new RuntimeException("Unable to read preferences", e);
		}
	}
	
	protected long getLong(String key, long defaultValue) {
		try {
			long def = PrefsPropsUtil.getLong(key, defaultValue);
			return PrefsPropsUtil.getLong(companyId, key, def);
		} catch (Exception e) {
			throw new RuntimeException("Unable to read preferences", e);
		}
	}

	/**
	 * Obtains web application base URL including context path.
	 * @param request
	 * @return
	 */
	private String getRequestUrl(HttpServletRequest request) {
		StringBuffer urlSB = new StringBuffer();
		
		urlSB.append(request.getScheme());
		urlSB.append("://");
		urlSB.append(request.getServerName());
		if ((request.getServerPort() != 80) && (request.getServerPort() != 443)) {
			urlSB.append(":");
			urlSB.append(request.getServerPort());
		}
		urlSB.append(request.getContextPath());
		
		return urlSB.toString();
	}
	
	@Override
	public String getSpAcsLocation() {
		return getSpBaseUrl() + SP_ACS_URI;
	}

	@Override
	public String getSpSloLocation() {
		return getSpBaseUrl() + SP_SLO_URI;
	}

	@Override
	public String getSpSloResponseLocation() {
		return getSpBaseUrl() + SP_SLO_URI;
	}

	@Override
	public String getSpEntityId() {
		return getString(PREF_SP_ENTITYID);
	}

	@Override
	public String getSpBaseUrl() {
		return baseUrl;
	}

	@Override
	public boolean isSpForceReAuthentication() {
		return getBoolean(PREF_SP_FORCE_REAUTHENTICATION, false);
	}

	@Override
	public String getSpAcsBinding() {
		return getString(PREF_SP_ACS_BINDING, SAMLConstants.SAML2_POST_BINDING_URI);
	}

	@Override
	public boolean isSpAcsWantsAssertionSigned() {
		return getBoolean(PREF_SP_FORCE_REAUTHENTICATION, false);
	}

	@Override
	public boolean isSpAcsWantsResponseSigned() {
		return getBoolean(PREF_SP_ACS_WANTS_RESPONSE_SIGNED, false);
	}

	@Override
	public String getSpSloBinding() {
		return getString(PREF_SP_SLO_BINDING, SAMLConstants.SAML2_REDIRECT_BINDING_URI);
	}

	@Override
	public boolean isSpWantsLogoutSigned() {
		return getBoolean(PREF_SP_SLO_WANTS_REQUEST_SIGNED, false);
	}

	@Override
	public String getSpNameIdFormat() {
		return getString(PREF_SP_NAME_ID_FORMAT, Saml2SpConfig.SAML_NAMEID_FORMAT_UNSPECIFIED);
	}

	@Override
	public String getSpLogoutCompletedUrl() {
		return getString(PREF_SP_LOGOUT_COMPLETED_URL, getSpBaseUrl());
	}

	@Override
	public Credential getSpSigningCredential() {
		String keystoreFilename = getString(PREF_KEYSTORE_FILE);
		String keystorePasswd = getString(PREF_KEYSTORE_PASSWD);
		String keyAlias = getString(PREF_SP_SIGN_KEY_ALIAS);
		String keyPasswd = getString(PREF_SP_SIGN_KEY_PASSWD);
		
		if (keystoreFilename == null || keyAlias == null) {
			return null;
		}
		
		return getKeystoreX509Credential(keystoreFilename, keystorePasswd,
				keyAlias, keyPasswd);		
	}

	private Credential getKeystoreX509Credential(String keystoreFilename,
			String keystorePasswd, String keyAlias, String keyPasswd) {
		KeyStore keystore;
		try {
			FileInputStream fis = new FileInputStream(keystoreFilename);
			keystore = KeyStore.getInstance("JKS");
			keystore.load(fis, keystore==null?null:keystorePasswd.toCharArray());
		} catch (Exception e) {
			log.warn("Unable to load keystore " + keystoreFilename, e);
			return null;
		}
		return new KeyStoreX509CredentialAdapter(keystore, keyAlias, keyPasswd==null?null:keyPasswd.toCharArray());
	}

	@Override
	public Credential getIdpSignatureValidationCredential() {
		String keystoreFilename = getString(PREF_TRUSTSTORE_FILE);
		String keyAlias = getString(PREF_IDP_CERT_ALIAS);
		
		if (keystoreFilename == null || keyAlias == null) {
			return null;
		}
		
		return getKeystoreX509Credential(keystoreFilename, null, keyAlias, null);		
	}
	
	@Override
	public long getSpMaxClockSkew() {
		return getLong(PREF_SP_MAX_CLOCK_SKEW, 5L);
	}

	@Override
	public String getIdpEntityId() {
		return getString(PREF_IDP_ENTITY_ID);
	}

	@Override
	public String getIdpSsoServiceLocation() {
		return getString(PREF_IDP_SSO_LOCATION);
	}

	@Override
	public String getIdpSsoServiceBinding() {
		return getString(PREF_IDP_SSO_BINDING);
	}

	@Override
	public boolean isIdpSsoWantsRequestsSigned() {
		return getBoolean(PREF_IDP_SSO_WANTS_REQUEST_SIGNED, false);
	}

	@Override
	public String getIdpArsServiceLocation() {
		return getString(PREF_IDP_ARS_LOCATION);
	}

	@Override
	public String getIdpSloServiceBinding() {
		return getString(PREF_IDP_SLO_BINDING);
	}

	@Override
	public String getIdpSloServiceRequestLocation() {
		return getString(PREF_IDP_SLO_REQUEST_LOCATION);
	}

	@Override
	public String getIdpSloServiceResponseLocation() {
		return getString(PREF_IDP_SLO_RESPONSE_LOCATION);
	}

	@Override
	public boolean isIdpSloWantsRequestsSigned() {
		return getBoolean(PREF_IDP_SLO_WANTS_REQUEST_SIGNED, false);
	}


	public String getRolesAttribute() {
		return getString(PREF_SP_SAMLATTR_ROLES);
	}
	
	public boolean isRolesAttributeValuesComaSeparated() {
		return getBoolean(PREF_SP_SAMLATTR_ROLES_COMA_SEPARATED, false);
	}

	public String getGroupsAttribute() {
		return getString(PREF_SP_SAMLATTR_GROUPS);
	}
	
	public boolean isGroupsAttributeValuesComaSeparated() {
		return getBoolean(PREF_SP_SAMLATTR_GROUPS_COMA_SEPARATED, false);
	}
	
	public String getFirstnameAttribute() {
		return getString(PREF_SP_SAMLATTR_FIRSTNAME);
	}
	
	public String getLastnameAttribute() {
		return getString(PREF_SP_SAMLATTR_LASTNAME);
	}
	
	public String getEmailAttribute() {
		return getString(PREF_SP_SAMLATTR_EMAIL);
	}

	public boolean isImportUser() {
		return getBoolean(PREF_IMPORT_USER, false);
	}
	
	public boolean isUpdateUser() {
		return getBoolean(PREF_UPDATE_USER, false);
	}

	public boolean isPublishMetadata() {
		return getBoolean(PREF_SP_PUBLISH_METADATA, true);
	}

	@Override
	public Integer getSpAttributeConsumingServiceIndex() {
		String strVal = getString(PREF_SP_ATTRIBUTE_CONSUMING_SERVICE_INDEX);
		if (strVal == null) {
			return null;
		}
		return Integer.valueOf(strVal);
	}

}
