/*
 * (C) Copyright 2013 drodak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package org.bezkres.liferay.saml2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.bezkres.saml2.util.Saml2Exception;
import org.bezkres.saml2.util.Saml2Util;
import org.opensaml.saml2.core.Assertion;

import com.liferay.portal.NoSuchUserException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.Group;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.security.auth.AutoLogin;
import com.liferay.portal.security.auth.AutoLoginException;
import com.liferay.portal.service.GroupLocalServiceUtil;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.service.persistence.GroupUtil;
import com.liferay.portal.service.persistence.RoleUtil;
import com.liferay.portal.util.PortalUtil;

public class Saml2AutoLogin implements AutoLogin {


	private static Log log = LogFactoryUtil.getLog(Saml2AutoLogin.class);	

	@Override
	public String[] login(HttpServletRequest request,
			HttpServletResponse response) throws AutoLoginException {

		try {
			log.debug("SAML autologin start");
			
			long companyId = PortalUtil.getCompanyId(request);
			LiferaySaml2SpConfig config = new LiferaySaml2SpConfig(request);
			
			Assertion assertion = null;
			HttpSession session = request.getSession(false);
			if (session != null) {
				assertion = (Assertion) session.getAttribute(LiferaySaml2SpConfig.ATTR_AUTOLOGIN_TOKEN);
			}
			if (assertion == null) {
				log.debug("SAML token not found. Autologin failed.");
				return null;
			}
			session.removeAttribute(LiferaySaml2SpConfig.ATTR_AUTOLOGIN_TOKEN);
			session.setAttribute(LiferaySaml2SpConfig.ATTR_ASSERTION, assertion);
			User user = null;
			
			String username = Saml2Util.getInstance().getSubjectName(assertion);
			try {
				user = UserLocalServiceUtil.getUserByScreenName(companyId, username);
			} catch (NoSuchUserException nsue) {
			}
			if (user == null) {
				if (log.isDebugEnabled()) {
					log.debug("User " + username + " not found");
				}
				if (config.isImportUser()) {
					if (log.isDebugEnabled()) {
						log.debug("Adding user " + username);
					}
					user = addUser(companyId, config, assertion);
				} else {
					
					return null;
				}
			} else if (config.isUpdateUser()) {
				if (log.isDebugEnabled()) {
					log.debug("Updating user " + username);
				}
				updateUser(companyId, user, config, assertion);
			}

			String credentials[] = new String[3];
			credentials[0] = String.valueOf(user.getUserId());
			credentials[1] = user.getPassword();
			credentials[2] = Boolean.toString(user.isPasswordEncrypted());
			return credentials;

		} catch (Exception e) {
			log.warn("Unable to do SAML autologin", e);
			return null;
		}

	}

	private User addUser(long companyId, LiferaySaml2SpConfig config, Assertion assertion) throws Saml2Exception, SystemException, PortalException {

		long creatorUserId = 0;
		Saml2Util saml2Util = Saml2Util.getInstance();
		String screenName = saml2Util.getSubjectName(assertion);
		String firstName = saml2Util.getFirstSAMLAttributeValue(assertion, config.getFirstnameAttribute());
		if (firstName == null) {
			firstName = screenName;
		}
		String lastName = saml2Util.getFirstSAMLAttributeValue(assertion, config.getLastnameAttribute());
		String emailAddress = saml2Util.getFirstSAMLAttributeValue(assertion, config.getEmailAttribute());
		if (emailAddress == null) {
			emailAddress = StringPool.BLANK;
		}
		Locale locale = LocaleUtil.getDefault();
		boolean autoPassword = true;
		boolean autoScreenName = false;
		long facebookId = 0;
		String openId = StringPool.BLANK;
		String middleName = StringPool.BLANK;
		int prefixId = 0;
		int suffixId = 0;
		boolean male = true;
		int birthdayMonth = Calendar.JANUARY;
		int birthdayDay = 1;
		int birthdayYear = 1970;
		String jobTitle = StringPool.BLANK;
		long[] groupIds = null;
		long[] organizationIds = null;
		
		
		boolean sendEmail = false;
		ServiceContext serviceContext = new ServiceContext();
		User user = UserLocalServiceUtil.addUser(creatorUserId, companyId,
				autoPassword, null, null, autoScreenName, screenName,
				emailAddress, facebookId, openId, locale, firstName,
				middleName, lastName, prefixId, suffixId, male, birthdayMonth,
				birthdayDay, birthdayYear, jobTitle, groupIds, organizationIds,
				new long[0], new long[0], sendEmail, serviceContext);

		updateUserRoles(companyId, user, config, assertion, saml2Util);
		
		updateUserGroups(companyId, user, config, assertion, saml2Util);

		return user;
	}

	
	private User updateUser(long companyId, User user, LiferaySaml2SpConfig config, Assertion assertion) throws Saml2Exception, SystemException, PortalException {
		Saml2Util saml2Util = Saml2Util.getInstance();
		String screenName = saml2Util.getSubjectName(assertion);
		String firstName = saml2Util.getFirstSAMLAttributeValue(assertion, config.getFirstnameAttribute());
		if (firstName == null) {
			firstName = screenName;
		}
		user.setFirstName(firstName);
		
		String lastName = saml2Util.getFirstSAMLAttributeValue(assertion, config.getLastnameAttribute());
		user.setLastName(lastName);
		
		String emailAddress = saml2Util.getFirstSAMLAttributeValue(assertion, config.getEmailAttribute());
		if (emailAddress == null) {
			emailAddress = StringPool.BLANK;
		}
		user.setEmailAddress(emailAddress);
		
		user = UserLocalServiceUtil.updateUser(user);		
		
		updateUserRoles(companyId, user, config, assertion, saml2Util);

		updateUserGroups(companyId, user, config, assertion, saml2Util);
		
		return user;
	}


	private void updateUserGroups(long companyId, User user,
			LiferaySaml2SpConfig config, Assertion assertion,
			Saml2Util saml2Util) throws SystemException, PortalException {
		String groupsAttrName = config.getGroupsAttribute();
		if (!StringUtils.isBlank(groupsAttrName)) {
			long[] groupIds = null;
			List<String> groupNamesList = saml2Util.getSAMLAttributeValues(assertion, groupsAttrName);
			if (config.isGroupsAttributeValuesComaSeparated()) {
				ArrayList<String> splittedGroupsList = new ArrayList<String>();
				for (String value: groupNamesList) {
					String valueRoles[] =  value.split(",");
					for (String g: valueRoles) {
						splittedGroupsList.add(g);
					}
				}
				groupNamesList = splittedGroupsList;
			}
			groupIds = findGroupIds(companyId, groupNamesList);
			long currentGroupIds[] = user.getGroupIds();
			Arrays.sort(groupIds);
			Arrays.sort(currentGroupIds);
			if (!Arrays.equals(groupIds, currentGroupIds)) {
				GroupLocalServiceUtil.addUserGroups(user.getUserId(), groupIds);
			}
		}
	}


	private void updateUserRoles(long companyId, User user,
			LiferaySaml2SpConfig config, Assertion assertion,
			Saml2Util saml2Util) throws SystemException, PortalException {
		String rolesAttrName = config.getRolesAttribute();
		if (!StringUtils.isBlank(rolesAttrName)) {
			long[] roleIds = null;
			List<String> roleNamesList = saml2Util.getSAMLAttributeValues(assertion, rolesAttrName);
			if (config.isRolesAttributeValuesComaSeparated()) {
				ArrayList<String> splittedRolesList = new ArrayList<String>();
				for (String value: roleNamesList) {
					String valueRoles[] =  value.split(",");
					for (String g: valueRoles) {
						splittedRolesList.add(g);
					}
				}
				roleNamesList = splittedRolesList;
			}
			
			roleIds = findRoleIds(companyId, roleNamesList);
			long currentRoleIds[] = user.getRoleIds();
			Arrays.sort(roleIds);
			Arrays.sort(currentRoleIds);
			if (!Arrays.equals(roleIds, currentRoleIds)) {
				RoleLocalServiceUtil.setUserRoles(user.getUserId(), roleIds);
			}
		}
	}

	private long[] findRoleIds(long companyId, List<String> roleNamesList) throws SystemException {
		Set<String> roleNamesSet = new HashSet<String>(roleNamesList);
		List<Role> roles = RoleUtil.findByCompanyId(companyId);
		List<Long> roleIdsList = new ArrayList<Long>(); 
		for (Role role : roles) {
			String roleName = role.getName();
			if (roleNamesSet.contains(roleName)) {
				roleIdsList.add(role.getRoleId());
			}
		}
		long ret[] = new long[roleIdsList.size()];
		int i = 0;
		for (Long id : roleIdsList) {
			ret[i++] = id;
		}
		return ret;
	}
	
	private long[] findGroupIds(long companyId, List<String> groupNamesList) throws SystemException {
		Set<String> groupNamesSet = new HashSet<String>(groupNamesList);
		List<Group> groups = GroupUtil.findByCompanyId(companyId);
		List<Long> groupIdsList = new ArrayList<Long>(); 
		for (Group group: groups) {
			String groupName = group.getName();
			if (groupNamesSet.contains(groupName)) {
				groupIdsList.add(group.getGroupId());
			}
		}
		
		long ret[] = new long[groupIdsList.size()];
		int i = 0;
		for (Long id : groupIdsList) {
			ret[i++] = id;
		}
		return ret;
	}
	
}
