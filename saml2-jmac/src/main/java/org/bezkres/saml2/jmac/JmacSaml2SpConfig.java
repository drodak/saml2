/*
 * (C) Copyright 2013 drodak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package org.bezkres.saml2.jmac;

import java.util.Map;

import javax.security.auth.callback.CallbackHandler;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.bezkres.saml2.util.Saml2SpConfig;
import org.opensaml.common.xml.SAMLConstants;
import org.opensaml.xml.security.credential.Credential;

public class JmacSaml2SpConfig implements Saml2SpConfig {

	private Map<String, String> properties;
	private CallbackHandler callbackHandler;
	private HttpServletRequest request;

	public JmacSaml2SpConfig(Map<String, String> properties,
			CallbackHandler callbackHandler, HttpServletRequest request) {
		super();
		this.properties = properties;
		this.callbackHandler = callbackHandler;
		this.request = request;
	}

	@Override
	public String getSpAcsLocation() {
		String baseUrl = getSpBaseUrl();
		String acsBinding = getSpAcsBinding();
		if (SAMLConstants.SAML2_POST_BINDING_URI.equals(acsBinding)) {
			return baseUrl + Saml2Constants.ACS_POST_URI;
		} else if (SAMLConstants.SAML2_ARTIFACT_BINDING_URI.equals(acsBinding)) {
			return baseUrl + Saml2Constants.ACS_ARTIFACT_URI;
		}
		return null;
	}

	@Override
	public String getSpSloLocation() {
		String baseUrl = getSpBaseUrl();
		String sloBinding = getSpSloBinding();
		if (SAMLConstants.SAML2_POST_BINDING_URI.equals(sloBinding)) {
			return baseUrl + Saml2Constants.SLO_POST_URI;
		} else if (SAMLConstants.SAML2_REDIRECT_BINDING_URI.equals(sloBinding)) {
			return baseUrl + Saml2Constants.SLO_REDIRECT_URI;
		}
		return null;
	}

	@Override
	public String getSpSloResponseLocation() {
		String baseUrl = getSpBaseUrl();
		String sloBinding = getSpSloBinding();
		if (SAMLConstants.SAML2_POST_BINDING_URI.equals(sloBinding)) {
			return baseUrl + Saml2Constants.SLO_POST_URI;
		} else if (SAMLConstants.SAML2_REDIRECT_BINDING_URI.equals(sloBinding)) {
			return baseUrl + Saml2Constants.SLO_REDIRECT_URI;
		}
		return null;
	}

	@Override
	public String getSpEntityId() {
		return getString("SpEntityId", getSpBaseUrl());
	}

	@Override
	public String getSpBaseUrl() {
		String spFrontendAddress = getString("SpFrontendAddress", null);
		if (spFrontendAddress == null) {
			spFrontendAddress = getRequestUrl(request);
		}
		return spFrontendAddress + request.getContextPath();
	}

	@Override
	public boolean isSpForceReAuthentication() {
		return getBoolean("SpForceReAuthentication", false);
	}

	@Override
	public String getSpAcsBinding() {
		return getString("SpAcsBinding", SAMLConstants.SAML2_POST_BINDING_URI);
	}

	@Override
	public boolean isSpAcsWantsAssertionSigned() {
		return getBoolean("SpAcsWantsAssertionSigned", true);
	}

	@Override
	public boolean isSpAcsWantsResponseSigned() {
		return getBoolean("SpAcsWantsResponseSigned", false);
	}

	@Override
	public String getSpSloBinding() {
		return getString("SpSloBinding",
				SAMLConstants.SAML2_REDIRECT_BINDING_URI);
	}

	@Override
	public boolean isSpWantsLogoutSigned() {
		return getBoolean("SpWantsLogoutSigned", false);
	}

	@Override
	public String getSpNameIdFormat() {
		return getString("SpNameIdFormat",
				Saml2Constants.SAML_NAMEID_FORMAT_UNSPECIFIED);
	}

	@Override
	public String getSpLogoutCompletedUrl() {
		return getString("SpLogoutCompletedUrl", null);
	}

	@Override
	public Credential getSpSigningCredential() {
		String spKeyAlias = getString("SpSigningKeyAlias", null);
		if (spKeyAlias == null) {
			return null;
		}
		JmacX509PrivateKey spSigningCredential = new JmacX509PrivateKey(
				callbackHandler, spKeyAlias);
		return spSigningCredential;
	}

	@Override
	public long getSpMaxClockSkew() {
		return getLong("SpSsoSessionMaxClockSkew", 5);
	}

	@Override
	public String getIdpEntityId() {
		return getString("IdpEntityId", null);
	}

	@Override
	public String getIdpSsoServiceLocation() {
		return getString("IdpSsoServiceLocation", null);
	}

	@Override
	public String getIdpSsoServiceBinding() {
		return getString("IdpSsoServiceBinding",
				SAMLConstants.SAML2_REDIRECT_BINDING_URI);
	}

	@Override
	public boolean isIdpSsoWantsRequestsSigned() {
		return getBoolean("IdpSsoWantsRequestsSigned", false);
	}

	@Override
	public String getIdpArsServiceLocation() {
		return getString("IdpArsServiceLocation", null);
	}

	@Override
	public String getIdpSloServiceBinding() {
		return getString("IdpSloServiceBinding", null);
	}

	@Override
	public String getIdpSloServiceRequestLocation() {
		return getString("IdpSloServiceRequestLocation", null);
	}

	@Override
	public String getIdpSloServiceResponseLocation() {
		return getString("IdpSloServiceResponseLocation", null);
	}

	@Override
	public boolean isIdpSloWantsRequestsSigned() {
		return getBoolean("IdpSloWantsRequestsSigned", false);
	}

	@Override
	public Credential getIdpSignatureValidationCredential() {
		String idpCertAlias = getString("IdpSigningCertAlias", null);
		if (idpCertAlias == null) {
			return null;
		}

		JmacX509Certificate idpCert = new JmacX509Certificate(callbackHandler,
				idpCertAlias);
		return idpCert;
	}

	@Override
	public Integer getSpAttributeConsumingServiceIndex() {
		return getInteger("SpAttributeConsumingServiceIndex");
	}

	private String getString(String key, String defaultValue) {
		String value = properties.get(key);
		return (value != null) ? value.trim() : defaultValue;
	}

	private boolean getBoolean(String key, boolean defaultValue) {
		String val = getString(key, Boolean.toString(defaultValue));
		return Boolean.valueOf(val);
	}

	private long getLong(String key, long defaultValue) {
		String val = getString(key, Long.toString(defaultValue));
		return Long.valueOf(val);
	}

	private Integer getInteger(String key) {
		String val = getString(key, null);
		return (val==null)?null:Integer.valueOf(val);
	}

	/**
	 * Obtains web application base URL including context path.
	 * 
	 * @param request
	 * @return
	 */
	private String getRequestUrl(HttpServletRequest request) {
		StringBuffer urlSB = new StringBuffer();

		urlSB.append(request.getScheme());
		urlSB.append("://");
		urlSB.append(request.getServerName());
		if ((request.getServerPort() != 80) && (request.getServerPort() != 443)) {
			urlSB.append(":");
			urlSB.append(request.getServerPort());
		}

		return urlSB.toString();
	}

	public boolean isPublishMetadata() {
		return getBoolean("SpPublishMetadata", true);
	}

	public String getSpGroupsAttributeName() {
		return getString("SpGroupsAttributeName", null);
	}

	public boolean isSpGroupsAttributeValuesComaSeparated() {
		return getBoolean("SpGroupsAttributeValuesComaSeparated", false);
	}
	
	public String[] getSpDefaultGroups() {
		String defaultGroups = getString("SpDefaultGroups", null);
		if (StringUtils.isBlank(defaultGroups)) {
			return new String[0];
		}
		return defaultGroups.split(",");

	}

	public long getSpSessionLifetime() {
		return getLong("SpSessionLifetime", 0);
	}

}
