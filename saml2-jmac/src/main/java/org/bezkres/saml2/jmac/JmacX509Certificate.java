/*
 * (C) Copyright 2013 drodak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package org.bezkres.saml2.jmac;

import java.security.AccessController;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.PrivilegedAction;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.message.callback.TrustStoreCallback;

import org.opensaml.xml.security.credential.BasicCredential;
import org.opensaml.xml.security.x509.X509Credential;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JmacX509Certificate extends BasicCredential implements X509Credential {

	private static Logger log = LoggerFactory.getLogger(JmacX509Certificate.class);
	
	private CallbackHandler callbackHandler;
	private String alias;
	private boolean initialized = false;
	private Collection<X509Certificate> entityCertificateChain;
	private X509Certificate entityCertificate;
	
	public JmacX509Certificate(CallbackHandler callbackHandler, String alias) {
		super();
		this.callbackHandler = callbackHandler;
		this.alias = alias;
	}


	private void populateCredential() {
        if (!initialized) {
        	KeyStore trustStore = AccessController.doPrivileged(new PrivilegedAction<KeyStore>() {
				@Override
				public KeyStore run() {
		        	try {
		            	final TrustStoreCallback tsCb = new TrustStoreCallback();
		        		callbackHandler.handle(new Callback[] {tsCb});
				        KeyStore trustStore = tsCb.getTrustStore();
		        		return trustStore;
		        	} catch (Exception e) {
		        		log.warn("Error retrieving trust store", e);
		        		return null;
		        	}
				}
        	});
        	Certificate cert;
	        try {
				cert = trustStore.getCertificate(alias);
			} catch (KeyStoreException e) {
        		log.warn("Error retrieving certificate with alias: " + alias, e);
				return;
			}
	        
	        if (cert != null) {
	            if (cert instanceof X509Certificate) {
                    entityCertificate = (X509Certificate) cert;
    	            Collection<X509Certificate> certCollection = new ArrayList<X509Certificate>();
    	            certCollection.add(entityCertificate);
    	            entityCertificateChain = certCollection;
	            }
	        }
	        initialized = true;
        }
	}
	
	
	@Override
	public X509Certificate getEntityCertificate() {
		populateCredential();
		return entityCertificate;
	}

	@Override
	public Collection<X509Certificate> getEntityCertificateChain() {
		populateCredential();
		return entityCertificateChain;
	}

	@Override
	public Collection<X509CRL> getCRLs() {
		return Collections.EMPTY_LIST;
	}

	@Override
	public PublicKey getPublicKey() {
		populateCredential();
		X509Certificate cert = getEntityCertificate();
		return cert!=null?cert.getPublicKey():null;
	}
	
}
