/*
 * (C) Copyright 2013 drodak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package org.bezkres.saml2.jmac;

public interface Saml2Constants {

	public static final String ATTR_AUTH_CONFIG_PROVIDER = "authConfigProvider";
	public static final String SSO_COOKIE_NAME = "SAML2_SESSION";
	public static final String SAML_NAMEID_FORMAT_UNSPECIFIED = "urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified";
	public static final String SAML_CONFIRMATION_BEARER = "urn:oasis:names:tc:SAML:2.0:cm:bearer";
	
	public static final String ACS_POST_URI = "/saml2/acsPost";
	public static final String ACS_ARTIFACT_URI = "/saml2/acsArtifact";
	public static final String METADATA_URI = "/saml2/metadata";
	public static final String ARS_SOAP_URI = "/saml2/arsSoap";
	public static final String SLO_REDIRECT_URI = "/saml2/sloRedirect";
	public static final String SLO_POST_URI = "/saml2/sloPost";
	public static final String LOGOUT_URI = "/saml2/logout";
	
}
