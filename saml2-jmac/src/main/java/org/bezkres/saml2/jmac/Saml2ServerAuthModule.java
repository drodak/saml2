/*
 * (C) Copyright 2013 drodak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package org.bezkres.saml2.jmac;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.security.auth.Subject;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;
import javax.security.auth.message.AuthException;
import javax.security.auth.message.AuthStatus;
import javax.security.auth.message.MessageInfo;
import javax.security.auth.message.MessagePolicy;
import javax.security.auth.message.callback.CallerPrincipalCallback;
import javax.security.auth.message.callback.GroupPrincipalCallback;
import javax.security.auth.message.module.ServerAuthModule;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.bezkres.saml2.util.Saml2Exception;
import org.bezkres.saml2.util.Saml2SpConfig;
import org.bezkres.saml2.util.Saml2Util;
import org.joda.time.DateTime;
import org.opensaml.common.xml.SAMLConstants;
import org.opensaml.saml2.core.Assertion;
import org.opensaml.saml2.core.AuthnStatement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;

public class Saml2ServerAuthModule implements ServerAuthModule {

	private static final String SAML2_SESSION_ATTRNAME = "SAML2_SESSION";
	private static final long MILLIS_PER_MINUTE = 60 * 1000;
	
	private static Class[] supportedMessageTypes = new Class[] {HttpServletRequest.class, HttpServletResponse.class};
	private static Logger log = LoggerFactory.getLogger(Saml2ServerAuthModule.class);

	private MessagePolicy requestPolicy;
	private MessagePolicy responsePolicy;
	private CallbackHandler callbackHandler;
	private Map<String, String> properties;
	
	public Saml2ServerAuthModule() {
	}
	
	@Override
	public void initialize(MessagePolicy requestPolicy, MessagePolicy responsePolicy,
			CallbackHandler callbackHandler, Map properties) throws AuthException {
		this.requestPolicy = requestPolicy;
		this.responsePolicy = responsePolicy;
		this.callbackHandler = callbackHandler;
		this.properties = properties;
	}

	@Override
	public AuthStatus validateRequest(MessageInfo messageInfo, Subject clientSubject, Subject serviceSubject) 
			throws AuthException {

		HttpServletRequest request = (HttpServletRequest) messageInfo.getRequestMessage();
		HttpServletResponse response = (HttpServletResponse)messageInfo.getResponseMessage();

		String requestUri = request.getRequestURI(); 
		String contextPath = request.getContextPath();

		Saml2Util saml2Util = Saml2Util.getInstance();

		JmacSaml2SpConfig config = new JmacSaml2SpConfig(properties, callbackHandler, request);
		
		if (requestUri.equals(contextPath + Saml2Constants.METADATA_URI)) {
			try {
				if (config.isPublishMetadata()) {
					saml2Util.sendMetadata(config, request, response);
					return AuthStatus.SEND_SUCCESS;
				} else {
					sendError(response, HttpServletResponse.SC_NOT_FOUND);
					return AuthStatus.FAILURE;
				}
				
			} catch (Exception e) {
				log.error("Error procesing metadata request", e);
				sendError(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				return AuthStatus.FAILURE;
			}
		}
		
		if (requestUri.equals(contextPath + Saml2Constants.ACS_POST_URI)
				&& SAMLConstants.SAML2_POST_BINDING_URI.equals(config.getSpAcsBinding())) {
			try {
				Assertion assertion = saml2Util.processAcsPost(config, request, response);
				createSaml2Session(saml2Util, config, assertion, request);
				redirectAfterAcs(config, request, response);
				return AuthStatus.SEND_CONTINUE;
			} catch (Exception e) {
				log.debug("Error processing ACS POST", e);
				sendError(response, HttpServletResponse.SC_UNAUTHORIZED);
				return AuthStatus.FAILURE;
			}
		}
		if (requestUri.equals(contextPath + Saml2Constants.ACS_ARTIFACT_URI)
				&& SAMLConstants.SAML2_ARTIFACT_BINDING_URI.equals(config.getSpAcsBinding())) {
			try {
				Assertion assertion = saml2Util.processAcsArtifact(config, request, response);
				createSaml2Session(saml2Util, config, assertion, request);
				redirectAfterAcs(config, request, response);
				return AuthStatus.SEND_CONTINUE;
			} catch (Exception e) {
				log.debug("Error processing ACS Artifact Request", e);
				sendError(response, HttpServletResponse.SC_UNAUTHORIZED);
				return AuthStatus.SEND_FAILURE;
			}
		}
		if (requestUri.equals(contextPath + Saml2Constants.LOGOUT_URI)) {
			try {
				Saml2Session saml2Session = getSaml2Session(config, request);
				if (saml2Session != null) {
					List<String> sessionIndices = new ArrayList<String>();
					if (saml2Session.getSessionIndex() != null) {
						sessionIndices.add(saml2Session.getSessionIndex());
					}
					saml2Util.sendLogoutRequest(config, saml2Session.getUsername(), 
							sessionIndices, request, response);
					request.getSession().invalidate();
					return AuthStatus.SEND_CONTINUE;
				} else {
					String afterLogoutUrl = config.getSpLogoutCompletedUrl();
					if (afterLogoutUrl == null) {
						afterLogoutUrl = config.getSpBaseUrl();
					}
					HttpSession session = request.getSession(false);
					if (session != null) {
						session.invalidate();
					}
					response.sendRedirect(afterLogoutUrl);
					return AuthStatus.SEND_CONTINUE;
				}
			} catch (Exception e) {
				log.debug("Logout error", e);
				sendError(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				return AuthStatus.SEND_FAILURE;
			}
		}
		
		if (requestUri.equals(contextPath + Saml2Constants.SLO_POST_URI)
				&& SAMLConstants.SAML2_POST_BINDING_URI.equals(config.getSpSloBinding())) {
			try {
				saml2Util.processSloPost(config, request, response);
				return AuthStatus.SEND_CONTINUE;
			} catch (Exception e) {
				log.debug("Error processing SLO POST Request", e);
				sendError(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				return AuthStatus.SEND_FAILURE;
			}
		}
		if (requestUri.equals(contextPath + Saml2Constants.SLO_REDIRECT_URI)
				&& SAMLConstants.SAML2_REDIRECT_BINDING_URI.equals(config.getSpSloBinding())) {
			try {
				saml2Util.processSloRedirect(config, request, response);
				return AuthStatus.SEND_CONTINUE;
			} catch (Exception e) {
				log.debug("Error processing SLO Redirect Request", e);
				sendError(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				return AuthStatus.SEND_FAILURE;
			}
		}
		
		// check if there is valid SSO session
		Saml2Session saml2Session = getSaml2Session(config, request);
		if (saml2Session != null) {
			try {
				setupSubject(messageInfo, clientSubject, saml2Session.getUsername(), 
						saml2Session.getGroups(), saml2Session.getAssertion());
			} catch (Exception e) {
				log.debug("Error setting subject", e);
				sendError(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				throw new AuthException("Error setting subject");
			}
			return AuthStatus.SUCCESS;
		}
		
		if (requestPolicy.isMandatory()) {
			// send AuthnRequest
			try {
				StringBuffer requestUrlSB = request.getRequestURL();
				if (request.getQueryString() != null) {
					requestUrlSB.append("?");
					requestUrlSB.append(request.getQueryString());
				}
				saml2Util.sendAuthnRequest(config, request, response, requestUrlSB.toString());
				return AuthStatus.SEND_CONTINUE;
			} catch (Exception e) {
				log.error("Error sending AuthnRequest", e);
				sendError(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				return AuthStatus.SEND_FAILURE;
			}
		} else {
			return AuthStatus.SUCCESS;
		}
	}

	@Override
	public AuthStatus secureResponse(MessageInfo messageInfo,
			Subject serviceSubject) throws AuthException {
		return AuthStatus.SUCCESS;
	}


	@Override
	public void cleanSubject(MessageInfo messageInfo, Subject subject)
			throws AuthException {
		// nothing to do
	}


	@Override
	public Class[] getSupportedMessageTypes() {
		return this.supportedMessageTypes;
	}
	
	/**
	 * Sends an HTTP error.
	 * @param response
	 * @param errorCode
	 * @throws AuthException
	 */
	private void sendError(HttpServletResponse response, int errorCode) throws AuthException {
		try {
			response.sendError(errorCode);
		} catch (IOException e) {
			log.error("Exception caught while sending error", e);
			throw new AuthException("Exception caught while sending error");
		}
	}

	
	private void redirectAfterAcs(Saml2SpConfig config,
			HttpServletRequest request, HttpServletResponse response)
			throws Saml2Exception {
		String redirectUrl = request.getParameter("RelayState");
		try {
			URL url = new URL(redirectUrl);
		} catch (MalformedURLException e) {
			redirectUrl = null;
		}
		if (redirectUrl == null) {
			redirectUrl = config.getSpBaseUrl();
		}
		try {
			response.sendRedirect(redirectUrl);
		} catch (IOException e) {
			throw new Saml2Exception("Redirect error", e);
		}
	}
	
	/**
	 * Populate the subject with principals.
	 * @param messageInfo
	 * @param clientSubject
	 * @param username
	 * @param groups
	 * @param assertion
	 * @throws IOException
	 * @throws UnsupportedCallbackException
	 */
	private void setupSubject(MessageInfo messageInfo, Subject clientSubject,
			String username, String groups[], Element assertion) throws IOException,
			UnsupportedCallbackException {
		clientSubject.getPrivateCredentials().add(assertion);
		callbackHandler.handle(new Callback[] { new CallerPrincipalCallback(clientSubject, username) });
		if (groups != null || groups.length > 0) {
			callbackHandler.handle(new Callback[] { new GroupPrincipalCallback(clientSubject, groups) });
		}
		messageInfo.getMap().put("javax.servlet.http.authType", "Saml2ServerAuthModule");
	}
	
	private Saml2Session createSaml2Session(Saml2Util saml2Util, JmacSaml2SpConfig config, 
			Assertion assertion, HttpServletRequest request)
			throws Saml2Exception, IOException {
		
		// setup a SAML session object
		Saml2Session saml2Session = new Saml2Session();
		saml2Session.setTimeCreated(System.currentTimeMillis());
		saml2Session.setAssertion(assertion.getDOM());
		String username = saml2Util.getSubjectName(assertion);
		saml2Session.setUsername(username.trim());
		String groups[];
		String groupsAttributeName = config.getSpGroupsAttributeName();
		if (!StringUtils.isBlank(groupsAttributeName)) {
			List<String> groupList = saml2Util.getSAMLAttributeValues(assertion, groupsAttributeName);
			if (config.isSpGroupsAttributeValuesComaSeparated()) {
				ArrayList<String> splittedGroupsList = new ArrayList<String>();
				for (String value: groupList) {
					String valueGroups[] =  value.split(",");
					for (String g: valueGroups) {
						splittedGroupsList.add(g);
					}
				}
				groups = splittedGroupsList.toArray(new String[splittedGroupsList.size()]);
			} else {
				groups = groupList.toArray(new String[groupList.size()]);
			}
		} else {
			groups = config.getSpDefaultGroups();	
		}
		saml2Session.setGroups(groups);
		for (AuthnStatement authnStmt: assertion.getAuthnStatements()) {
			saml2Session.setSessionIndex(authnStmt.getSessionIndex());
			DateTime sessionNotOnOrAfter = authnStmt.getSessionNotOnOrAfter();
			saml2Session.setSessionNotOnOrAfter(sessionNotOnOrAfter !=null?sessionNotOnOrAfter.getMillis():Long.MAX_VALUE);
		}
		
		// store SAML session object in HTTP session
		HttpSession session = request.getSession(true);
		session.setAttribute(SAML2_SESSION_ATTRNAME, saml2Session);
		
		return saml2Session;
	}

	
	/**
	 * Returns valid SSO session object.
	 * @param ssoSessionId
	 * @return valid SSO session or null if not found or session is invalidated.
	 */
	private Saml2Session getSaml2Session(JmacSaml2SpConfig config, HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		if (session == null) {
			return null;
		}
		
		Object samlSessionObject = session.getAttribute(SAML2_SESSION_ATTRNAME);
		if (samlSessionObject == null || !(samlSessionObject instanceof Saml2Session)) {
			return null;
		}
		Saml2Session saml2Session = (Saml2Session)samlSessionObject;
		long currentTime = System.currentTimeMillis();
		long sessionLifetime = config.getSpSessionLifetime(); 
		if (sessionLifetime == 0) {
			// check SessionNotOnOrAfter parameter defined in assertion  
			if (currentTime - config.getSpMaxClockSkew() * MILLIS_PER_MINUTE > saml2Session.getSessionNotOnOrAfter()) {
				return null;
			}
		} else if (sessionLifetime > 0) {
			// check against configured session lifetime
			if (currentTime - config.getSpMaxClockSkew() * MILLIS_PER_MINUTE > 
				saml2Session.getTimeCreated() + sessionLifetime * MILLIS_PER_MINUTE) {
				return null;
			}
		}
		return saml2Session;
	}

}
